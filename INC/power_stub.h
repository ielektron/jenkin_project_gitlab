
uint8_t M_ADC_CHsts_get(uint8_t ch_no, uint16_t *buffer);
G_RESP_CODE NOTI_hdlr(E_SYS_EVENTS event, uint8_t status);
void APP_eventManager(E_SYS_EVENTS event, uint8_t sts);
void  pow_han_input(uint16_t ac_val, uint16_t bat_val);
uint16_t  pow_han_output_ac(void);
uint16_t  pow_han_output_bat(void);
void pow_init_def(void);
