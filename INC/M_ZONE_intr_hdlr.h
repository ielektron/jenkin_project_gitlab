#ifndef M_ZONE_HANDLER_H
#define M_ZONE_HANDLER_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "M_mode_key_handler.h"

#define TAMP_DEFAULT NC
//#define TAMP_DEFAULT NO
#define ZONE_COUNT 4

#define LOG_RTC_DL ';'
#define ZONE_DELIMITER '\r'
#define LINE_DELIMITER '\r'
#define STS_DELAY_DELIMITER ',' 
 /********************PIN DEFINITIONS****************/ 
 
 /********************PIN DEFINITIONS****************/
 typedef enum
{
	NO = 0,//dont change
	NC = 1,//these values
	ZONE_TRIG_END
}ZONE_TRIG;

typedef enum
{
	NO_ALT = 0,//dont change 
	ALT_SENSED = 1,//the 
	ALT_TRIGGERED = 2,//order
	NIGHT_WAIT = 3
}ZONE_STS;
 
 typedef struct  
{
	ACTIVE_STS active_sts;
	ZONE_TRIG trig_setting;
	uint16_t delay_timing;
	SYS_MODE_E mode_setting;
	ZONE_TRIG zone_input;
	uint16_t zone_dly_cnt;
	ZONE_STS zone_sts;
	uint8_t db_cnt;
}ZONE_SETTINGS;
 
 extern ZONE_SETTINGS sensor_index[ZONE_COUNT];	
  
G_RESP_CODE m_vZONE_monitor(void);
uint8_t m_u8ZONE_sts_get(uint8_t zone); 
void m_u8ZONE_reset(void); 
uint8_t* ZONE_LogStore(uint8_t *arr);
void ZONE_StsStore(uint8_t *arr,E_SYS_EVENTS command);
#endif
