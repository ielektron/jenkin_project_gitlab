#include "M_ADC_hdlr.h" 
#include "M_power_handler.h"
#include "M_SYS_NOTI_hdlr.h"
#include "A_main_app_handler.h"
#include "M_ZONE_intr_hdlr.h"

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
/*
	bat 12v --> 440 to 470
	bat 10v --> 360 to 385
	bat 9v --> 325 to 340
	
*/

/*#define AC_ON_TH 534
#define BAT_ON_TH 450
#define BAT_LOW_TH 500
#define BAT_SH_TH 500
#define BAT_FAIL_TH 500*/

/******************COLLECTED DATA*************
AC SMPS(15.3V)- 537
BAT 13V - 462 
BAT 12V - 422
BAT 11V - 387
BAT 10.5V - 367
BAT 10V - 348
**********************************************/

#define AC_ON_TH 500

#define BAT_NORM_H 420
#define BAT_NORM_L1 320
#define BAT_NORM_L2 365

#define BAT_LOW_H 315
#define BAT_LOW_L1 280
#define BAT_LOW_L2 300

#define BAT_SH 250

#define BAT_CONN_TH 815
#define BAT_DISCONN_TH 810

 #define POW_TH 20
 
 #define BAT_LOW_DEV 30



	POW_MODE_T POW_sts = ES_DEFAULT;
	POW_MODE_T BAT_sts = ES_DEFAULT;

  
 
void m_POW_monitor(void)
{	
	uint16_t buffer = 0;
	POW_MODE_T sts = POW_sts;
	if(ADC_STABLE == M_ADC_CHsts_get(ADC_AC,&buffer))
	{
		if((AC_ON_TH) <= buffer)
		{
			  sts = POW_AC;
		}
		else if((BAT_NORM_L1 <= buffer) && (BAT_NORM_H > buffer))
		{
			if((POW_sts == BAT_SHUT)||(POW_sts == BAT_LOW))
			{
				if(BAT_NORM_L2 < buffer)
				{
					sts = BAT_NORM;
				}
			}
			else
			{
				sts = BAT_NORM;
			}
			
		}
		else if((BAT_LOW_L1 <= buffer) && (BAT_LOW_H > buffer))
		{
			if(POW_sts == BAT_SHUT)
			{
				if(BAT_LOW_L2 < buffer)
				{
					sts = BAT_LOW;
				}
			}
			else
			{
				sts = BAT_LOW;
			}			 
		}
		else if(BAT_SH > buffer)
		{
			  sts = BAT_SHUT;				
		}		
		if(POW_sts - sts)
		{
			POW_sts = sts;
			NOTI_hdlr(E_POWER,POW_sts);			 
			APP_eventManager(E_POWER,POW_sts);
			/**********##TBH##*****GSM MUST BE TURNED OFF FOR POW_SHUT****/
		}
	}	
	sts = BAT_sts;
	if(ADC_STABLE == M_ADC_CHsts_get(ADC_BAT,&buffer))
	{
		/******************logic must be verified with different battery voltages*********************/
		if(BAT_CONN_TH <= buffer)
		{
			sts = BAT_DISCONN;	
		
		}
		else if(BAT_DISCONN_TH >= buffer)
		{
			sts = BAT_CONN;
		}
	}	
	if(BAT_sts - sts)
	{
		BAT_sts = sts;		 
		NOTI_hdlr(E_POWER,BAT_sts);
	}	 
}
void m_POW_reset(void)
{
	POW_sts = ES_DEFAULT;
}
  
void pow_init(void)
{
	POW_sts = ES_DEFAULT;
	BAT_sts = ES_DEFAULT;
}

