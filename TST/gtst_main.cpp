
/*

filename : gtst_main.cpp
author : pallavi
created at : 2019-06-06 17:03:15.087629

*/
    

#include <stdio.h>
#include "gtest/gtest.h"

GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from gtest_main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
        