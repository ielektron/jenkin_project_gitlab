#include "M_ADC_hdlr.h" 
#include "M_power_handler.h"
#include "M_SYS_NOTI_hdlr.h"
#include "A_main_app_handler.h"
#include "power_stub.h"


uint16_t Global_ac_val;
uint16_t Global_bat_val;
POW_MODE_T Global_output_ac;
POW_MODE_T Global_output_bat;

uint8_t M_ADC_CHsts_get(uint8_t ch_no, uint16_t *buffer)
{
	if (ch_no == ADC_AC)
	{
		*buffer = Global_ac_val;
	}
	else if (ch_no == ADC_BAT)
	{
		*buffer = Global_bat_val;
	}
	return ADC_STABLE;
}

G_RESP_CODE NOTI_hdlr(E_SYS_EVENTS event, uint8_t status)
{
	if (status <= BAT_SHUT)
	{
		Global_output_ac = (POW_MODE_T)status;
	}
	else if (status > BAT_SHUT)
	{
		Global_output_bat = (POW_MODE_T)status;
	}

   
}


void APP_eventManager(E_SYS_EVENTS event, uint8_t sts)
{

}

void  pow_han_input(uint16_t ac_val, uint16_t bat_val)
{
	Global_ac_val = ac_val;
	Global_bat_val = bat_val;

}

uint16_t  pow_han_output_ac(void)
{

	return Global_output_ac;
	
}
uint16_t  pow_han_output_bat(void)
{
	return Global_output_bat;
}

void pow_init_def(void)
{
	Global_output_ac = ES_DEFAULT;
	Global_output_bat = ES_DEFAULT;
}




















/*
 int Temp_buffer=0; 
 int Temp_power_sts;
uint8_t M_ADC_CHsts_get(uint8_t ch_no, uint16_t *buffer)
{
	 *buffer=459;
}

G_RESP_CODE NOTI_hdlr(E_SYS_EVENTS event, uint8_t status)
{
	Temp_power_sts = status;
	getsts(Temp_power_sts);
}
*/
