
/*

filename : gtst_Jenkin_Project.cpp
author : Pallavi
created at : 2019-06-06 12:27:14.365407

*/
    
# include "gtest/gtest.h"

extern "C"
{
#include "M_power_handler.h"
#include "M_power_handler.c"
#include "power_stub.h"
#include "power_stub.c"
}

class POW_HANDLER : public::testing::Test 
{
protected:

	void SetUp()
	{
       pow_init();
	   pow_init_def();     
    }
	void TearDown()
	{
		
	}
};
TEST_F(POW_HANDLER, sts_bat_con)
{
	TEST_ID("1.1");
    TEST_DESC("BATTERY CONNECTED");
	pow_han_input(0,800);
	m_POW_monitor();
	EXPECT_EQ(BAT_CONN, pow_han_output_bat());
	
}


TEST_F(POW_HANDLER, sts_bat_nor)
{
	TEST_ID("1.2");
    TEST_DESC("BATTERY NORMAL");
	pow_han_input(391, 0);
	m_POW_monitor();
	EXPECT_EQ(BAT_NORM, pow_han_output_ac());

}

TEST_F(POW_HANDLER, sts_bat_pow)
{
	TEST_ID("1.3");
    TEST_DESC("POW AC");
	pow_han_input(500, 0);
	m_POW_monitor();
	EXPECT_EQ(POW_AC, pow_han_output_ac());

}

TEST_F(POW_HANDLER, sts_bat_dis)
{
	TEST_ID("1.4");
    TEST_DESC("BATTERY DISCONNECTED");
	pow_han_input(0, 817);
	m_POW_monitor();
	EXPECT_EQ(BAT_DISCONN, pow_han_output_bat());

}

TEST_F(POW_HANDLER, sts_bat_low)
{
	
    
    TEST_ID("1.5");
    TEST_DESC("BATTERY LOW");
	pow_han_input(290, 0);
	m_POW_monitor();
	EXPECT_EQ(BAT_LOW, pow_han_output_ac());

}

TEST_F(POW_HANDLER, sts_bat_sh)
{
	TEST_ID("1.6");
    TEST_DESC("BATTERY SHUT");
	pow_han_input(230, 0);
	m_POW_monitor();
	EXPECT_EQ(BAT_SHUT, pow_han_output_ac());
	

}

TEST_F(POW_HANDLER, sts_bat_def)
{
	
    TEST_ID("1.7");
    TEST_DESC("DEFAULT RES");
	pow_han_input(0, 812);
	m_POW_monitor();
	EXPECT_EQ(ES_DEFAULT, pow_han_output_bat());

}



TEST_F(POW_HANDLER,bat_sts_norm)
{
	TEST_ID("1.8");
    TEST_DESC("BATTERY NORMAL");

	pow_han_input(350, 0);
	m_POW_monitor();
	EXPECT_EQ(BAT_NORM, pow_han_output_ac());

}

TEST_F(POW_HANDLER, bat_sts_esdef)
{
	TEST_ID("1.9");
    TEST_DESC("DEFAULT RES");

	pow_han_input(450, 0);
	m_POW_monitor();
	EXPECT_EQ(ES_DEFAULT, pow_han_output_ac());

}

TEST_F(POW_HANDLER, bat_sts_powchck)
{
	TEST_ID("1.10");
    TEST_DESC("BATTERY NORMAL");
    pow_han_input(240, 0);
	m_POW_monitor();

	pow_han_input(366, 0);
	m_POW_monitor();
	EXPECT_EQ(BAT_NORM, pow_han_output_ac());
}

TEST_F(POW_HANDLER, bat_sts_powlow)
{
	TEST_ID("1.11");
    TEST_DESC("BATTERY NORMALLY CONNECTED");
	pow_han_input(285, 0);
	m_POW_monitor();
	
	pow_han_input(410, 0);
	m_POW_monitor();
	EXPECT_EQ(BAT_NORM, pow_han_output_ac());
}


TEST_F(POW_HANDLER, bat_sts_powshut)
{
	TEST_ID("1.12");
    TEST_DESC("BATTERY LOW");
	pow_han_input(240, 0);
	m_POW_monitor();
	
	pow_han_input(310, 0);
	m_POW_monitor();
	EXPECT_EQ(BAT_LOW, pow_han_output_ac());
}




